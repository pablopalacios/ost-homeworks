#!/usr/bin/local/python3
"""String methods in functions"""

import sys

def capitalize(text):
    """capitalize a text"""
    return text.capitalize()
def title(text):
    """make a text title cased"""
    return text.title()
def upper(text):
    """make a text upper cased"""
    return text.upper()
def lower(text):
    """make a text lower cased"""
    return text.lower()
def quit(text):
    """Quit the program"""
    print('Goodbye for now')
    sys.exit()

if __name__=="__main__":
    functions = {
        'capitalize':capitalize,
        'title':title,
        'upper':upper,
        'lower':lower,
        'quit':quit
    }

    options = functions.keys()
    p = 'Enter a function name ({0}): '.format(', '.join(options))
    while True:
        i = input(p)
        option = functions.get(i, None)
        if option:
            print(option("python is a Dynamic LANGuage"))
        else:
            print('Please select a valid option!')
        
