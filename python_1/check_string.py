#!/usr/local/bin/python3
"""This is a program that verify if the input is in upper-case and ends with a period. \n"""
print(__doc__)

user_input = ""
is_upper  = user_input.isupper()
ends_with = user_input.endswith(".")

while is_upper == False or ends_with == False:
    user_input = input("Please, enter an upper-case string ending with a period: ")
    is_upper  = user_input.isupper(); ends_with = user_input.endswith(".")
    if is_upper == True and ends_with == True:
        print("Input meets both requirements.")
    else:
        if is_upper  == False:
            print("It is not all in upper-case.")
        if ends_with == False:
            print("It does not end with a period.")