#!/usr/local/bin/python3
"""A program to divide 10 by an user input"""

print('Dividing 10 by an integer')
while True:
    i = input('Provide an integer (press enter to quit the program): ')
    if not i:
        print('Quit')
        break
    try:
        result = 10/int(i)
        print(result)
    except ValueError:
        print('"{0}" is not an Integer!'.format(i))
    except ZeroDivisionError:
        print('You can\'t divide by zero')
