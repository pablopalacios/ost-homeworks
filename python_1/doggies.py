#!/usr/bin/python3
"""Just another OST homework about classes and Oobjects"""

class Dog:
    def __init__(self,name,breed):
        self.name = name
        self.breed = breed

    def __str__(self):
        return "{0}:{1}".format(self.name, self.breed) 

    def lst(dogs):
        print('DOGS')
        for i,dog in enumerate(dogs):
            print('{0}. {1}'.format(i+1,dog))

dogs = []
while True:
    n = input('Name: ')
    if not n:
        break
    b = input('Breed: ')
    dogs.append(Dog(n,b))
    Dog.lst(dogs)
    print('*'*40)

