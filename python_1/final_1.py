#!/usr/bin/python3
"""A class with some text methods"""

class Text:
    def __init__(self, file_name):
        """file_name must be a file name in the current directory"""
        self.file = open(file_name,'r')
        self.text = self.file.read()

    def word_list(self):
        """Remove all the ponctuations from a text"""
        punc = ('&-_,.!?;:\'"()\\/')
        for p in punc:
            if p in self.text:
                self.text = self.text.replace(p,'')
        return self.text

    def count(self):
        """Count the number of words per word length and print a list"""
        a = self.word_list().split()
        d = {}
        for word in a:
            l = len(word)
            d[l] = d.get(l,0)+1
        print('Length Count')
        for key in sorted(d):
            print('{0:<6} {1:<5}'.format(key,d[key]))
        
        
if __name__ == '__main__':
    a = Text('declaration.txt')
    a.count()

