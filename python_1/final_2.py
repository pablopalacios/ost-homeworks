#!/usr/local/bin/python3
"""A class with some text methods"""

class Text:
    def __init__(self, file_name):
        self.file = open(file_name,'r')
        self.text = self.file.read()

    def cleaning(self):
        """Remove all the ponctuations from a text file"""
        punc = ('&-_,.!?;:\'"()\\/')
        for p in punc:
            if p in self.text:
                clean_text = self.text.replace(p,'')
        self.file.close()
        return clean_text

    def count(self):
        """Counts the number of words and return it in dict where key is the length and value is the ocurrence"""
        d = {}
        clean_text = self.cleaning().split()
        for word in clean_text:
            l = len(word)
            d[l] = d.get(l,0)+1
        return d

    def table(self):
        """Prints a table from a dict with a length:count key:value"""
        d = self.count()
        bar = 22*'-'
        print(bar)
        print('|{0:^10}|{1:^9}|'.format('Length','Count'))
        print(bar)
        for key in sorted(d):
            print('|{0:>10}|{1:>9}|'.format(key,d[key]))
        print(bar)

    def histogram(self):
        """Prints a histogram from a dict with a length:count key:value"""

        def y (max_count):
            """Generates the higher point on y-axis based on the max value of x on y"""
            while max_count%10: # Generates the higher point
                max_count += 1
            return max_count

        def x (value,max_count):
            """Genarates horizontal histogram lines"""
            h_lines = []
            count = int((value/max_count)*20) # The value of count scaled for a 20 | y-axis hight
            empties = 20 - count 
            for unit in range(count):
                h_lines.append(' * ')
            for empty in range(empties):
                h_lines.append('   ')
            return h_lines

        def converter(h_lines, max_count):
            """Converts h_lines into vertical histogram lines"""
            v_lines = []
            index = 0
            while index != 20:
                v_line = []
                for h_line in h_lines:
                    v_line.append(h_line[index])
                v_lines.append(v_line)
                index += 1
            return v_lines

        # Set the dict
        d = self.count() 
        max_count = max(d.values())
        max_y = y(max_count)

        # Create the lines
        h_lines = []
        for count in d.values():
            h_lines.append(x(count,max_y))
        v_lines = converter(h_lines,max_y)

        # Let it print!
        grad_y = max_y 
        unit = int(grad_y/20)
        for v_line in v_lines[::-1]: # [::-1] or you will take a upside-down histogram ;)
            print('{0:>4} -| {1}'.format(grad_y,''.join(v_line)))
            grad_y -= unit

        bar = ['{0:^3}'.format('0')]
        for key in d.keys():
            grad_x = str(key)
            bar.append('{0:^3}'.format(grad_x))
        max_x = len(bar)+1
        print('{0:>4} {1}'.format('0',max_x*'-*-'))
        print('{0:>4} {1}'.format('',''.join(bar)))
        
if __name__ == '__main__':
    a = Text('declaration.txt')
    a.table()
    print('\n')
    a.histogram()
