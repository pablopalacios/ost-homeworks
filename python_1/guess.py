#!/usr/bin/python3
"""Another Guess Number Game"""

secret = 16
guess = 0
count = 0
while guess != secret:
    guess=int(input("Guess a number: "))
    count += 1
    if guess == secret:
        print("You win!")
    else:
        if guess > secret:
            print("Too High")
        else:
            print("Too Low")
        if count == 5:
            print("You Lose")
            break