#!/usr/local/bin/python3
"""A guessing number game!"""

from random import randint

secret = randint(1,99)

while True:
    guess = int(input("Guess a number: "))
    if guess == secret:
        print('You guessed it!')
        break
    else:
        if guess < secret:
            print('Too Low')
        else:
            print('Too High')
