#!/usr/local/bin/python3

a = ((1, 1), (2, 2), (12, 13), (4, 4))
for b,c in a:
    d=b*c
    print("{2:>4} = {1:>2} x {0:>2}".format(b,c,d))

