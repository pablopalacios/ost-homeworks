#!/usr/bin/python3
"""Another OST Homework"""

def book_title(title):
    """ Takes a string and returns a book title style string.
    All words EXCEPT for small words are made title case
    unless the string starts with a preposition, in which
    case the word is correctly captalized.

    >>> book_title('DIVE Into python')
    'Dive into Python'
    
    >>> book_title('the great gatsby')
    'The Great Gatsby'
    
    >>> book_title('the WORKS OF AleXANDer dumas')
    'The Works of Alexander Dumas'
    """
    prepositions = ('into', 'the', 'a', 'of', 'at', 'in', 'for', 'on')
    words = title.title().split() 
    for word in words[1:]:
        if word.lower() in prepositions:
            words[words.index(word)] = word.lower()
    return ' '.join(words)

def _test():
    import doctest, refactory
    return doctest.testmod(refactory)

if __name__ == "__main__":
    _test()
