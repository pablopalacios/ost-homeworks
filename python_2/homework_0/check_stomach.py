"""Verifies that each animal in animal table eats at least
one feed.
"""
import mysql.connector
from database import login_info

def verify_list(l):
    """Verifies if a list has at least one True value"""
    for i in l:
        if i:
            return True
    return False

db = mysql.connector.Connect(**login_info)
cursor = db.cursor()

if __name__ == "__main__":
    cursor.execute("SELECT id,name FROM animal")
    animals = {anid:name for anid,name in cursor.fetchall()}

    cursor.execute("SELECT anid, feed FROM food")
    table = cursor.fetchall()

    food = {}
    for anid, feed in table:
        food[anid] = food.get(anid,[]) + [feed]
        starving_animals = [anid for anid,food_list in food.items() \
                            if not verify_list(food_list)]

    for animal_id in animals.keys():
        if animal_id not in food.keys() and animal_id not in starving_animals:
            starving_animals.append(animal_id)
    if not starving_animals:
        print("All animals are satisfied.")
    else:
        for animal_id in starving_animals:
            print(animals[animal_id],"is starving and has nothing to eat.")