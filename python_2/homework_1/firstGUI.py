from tkinter import *

class Application(Frame):
    def __init__(self,master=None):
        Frame.__init__(self, master)
        self.pack()
        self.createWidgets()
        
    def createWidgets(self):
        self.v1 = Entry()
        self.v2 = Entry()
        self.l = Label(text="0")
        self.b = Button(text="Sum",command=self.sum_float)
        self.v1.pack()
        self.v2.pack()
        self.l.pack()
        self.b.pack()
        
    def sum_float(self):
        try:
            a = float(self.v1.get())
            b = float(self.v2.get())
            result = a+b
            self.l.config(text=result)
        except:
            self.l.config(text="***ERROR***")
    
root = Tk()
app = Application(master=root)
app.mainloop()