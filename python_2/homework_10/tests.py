import os
import shutil
import tempfile
import unittest
import zipfile
from zipper import zipper

class TestZipper(unittest.TestCase):
    def setUp(self):
        self.temp_dir = tempfile.mkdtemp()
        self.archive_dir = 'archive'
        self.path = os.path.join(self.temp_dir,self.archive_dir)
        os.mkdir(self.path)
        self.names = [os.path.join(self.archive_dir,name) for name in ['red','blue','green']]
        self.zipfile = zipfile.ZipFile(os.path.join(self.temp_dir,'fixture.zip'),'w')
        
        current_dir = os.getcwd()
        os.chdir(self.temp_dir)
        for name in self.names:
            f = open(name,'w')
            f.close()
            self.zipfile.write(name)
        os.chdir(current_dir)
        
    def test_without_directories(self):
        z = zipper(self.path)
        return self.assertEqual(set(z.namelist()), set(self.zipfile.namelist()))
    
    def test_with_directories(self):
        for name in self.names:
            directory = os.path.join(self.temp_dir,name)
            os.mkdir(directory+'_dir')
        z = zipper(self.path)
        return self.assertEqual(set(z.namelist()), set(self.zipfile.namelist()))

    def tearDown(self):
        self.zipfile.close()
        shutil.rmtree(self.temp_dir)

if __name__ =="__main__":
    unittest.main()