import zipfile
import os
import glob

def zipper(path):
    origin_dir= os.getcwd()
    parent_dir = os.path.dirname(path)
    root_dir = os.path.basename(path)
    os.chdir(parent_dir)
    names = glob.glob(root_dir+'/*')
    files = [name for name in names if os.path.isfile(name)]
    z = zipfile.ZipFile('zipper.zip','w')
    for file in files:
        z.write(file)
    z.close()
    os.chdir(origin_dir) # going back
    return z
