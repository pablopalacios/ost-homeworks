from email.mime.multipart import MIMEMultipart
from email.mime.nonmultipart import MIMENonMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
import mimetypes, os

def guess_type(fn):
    mimetypes.init()
    content_type = mimetypes.guess_type(fn)[0]
    return content_type
        
def make_attachment(fn):
    with open(fn,'rb') as fp:
        file = fp.read()
    content_type = guess_type(fn)
    if content_type:
        maintype, subtype = content_type.split('/')
        attachment = MIMENonMultipart(maintype,subtype)
        attachment.set_payload(file,charset='base64')
    else:
        attachment = MIMEApplication(file)
    attachment.add_header('Content-Disposition','attachment',filename=os.path.basename(fn))
    return attachment
        
def make_msg(receiver,text,files=[]):
    msg = MIMEMultipart()
    msg['to'] = receiver
    msg['subject'] = 'Python make_msg function'
    text = MIMEText(text,'plain')
    msg.attach(text)
    for file in files:
        attachment = make_attachment(file)
        msg.attach(attachment)
    return msg

if __name__ == "__main__":
    files = ['./eu.png', './msg.pdf','./file.unknown']
    msg = make_msg("plabo@sleepy.com", "This is my email message",files)
    print(msg.as_string())