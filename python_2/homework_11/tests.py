from make_email import make_msg, make_attachment, guess_type
from email.mime.image import MIMEImage
import unittest
    
class TestCase(unittest.TestCase):
    def setUp(self):
        self.files = ['./eu.png', './msg.pdf','file.unknown']
        self.to = 'pablo@sleepy.com.br'
        self.text = 'This is a text message.'
    
    def test_guess_type(self):
        files = self.files
        types = ['image/png', 'application/pdf',None]
        for file, content_type in zip(files,types):
            self.assertEqual(guess_type(file),content_type)
    
    def test_make_attachment(self):
        fn = self.files[0]
        att = make_attachment(fn)
        self.assertEqual(att['Content-Type'],'image/png; charset="base64"')
        self.assertEqual(att['Content-Transfer-Encoding'],'base64')
        self.assertEqual(att['Content-Disposition'],'attachment; filename="eu.png"')
        with open(fn,'rb') as fp:
            expected_payload = MIMEImage(fp.read()).get_payload()
        self.assertEqual(expected_payload, att.get_payload())
        
    def test_attachments_length(self):
        msg = make_msg(self.to,self.text,self.files)
        len_attachments = 0
        for m in msg.get_payload():
            if m['Content-Disposition'] and 'attachment' in m['Content-Disposition']:
                len_attachments+=1
        self.assertEqual(len(self.files),len_attachments)
            
    def test_attachment_integrity(self):
        with open(self.files[0],'rb') as fp:
            expected_payload = MIMEImage(fp.read()).get_payload()
        msg = make_msg(self.to,self.text,[self.files[0]])
        observed_payload = msg.get_payload()[1].get_payload()
        return self.assertEqual(expected_payload, observed_payload)
                
    def test_make_msg(self):
        msg = make_msg(self.to,self.text,self.files)
        self.assertEqual('multipart/mixed', msg['Content-Type'])
        self.assertEqual(self.to, msg['to'])
        self.assertEqual(self.text, msg.get_payload()[0].get_payload())
        
if __name__=="__main__":
    unittest.main()