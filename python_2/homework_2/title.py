"""A function to make strings titled"""

import unittest

def title(s):
    "Returns same string with first character capitalized."
    dont_title=('the','a','an','the','in','on','at','for','and') # and my english knowledge just let me remember these ones ;)
    pre_title = s.lower().split()
    final_title = []
    for word in pre_title:
        if word not in dont_title or word == pre_title[0]:
            final_word =word[0].upper()+word[1:].lower()
            final_title.append(final_word)
        else:
            final_title.append(word)
    return ' '.join(final_title)

class TestTitle(unittest.TestCase):
    
    def test_all_lower(self):
        s = 'yey' 
        self.assertEqual(title(s), 'Yey' , "The tile of yey should be Yey")
        
    def test_all_upper(self):
        s = 'YAY'
        self.assertEqual(title(s), 'Yay' , "The title of YAY should beb Yay")
        
    def test_many_words(self):
        s = 'python programming'
        self.assertEqual(title(s), 'Python Programming' , "The title of python programming should be Python Programming")

    def test_prepositions_and_articles(self):
        s = 'a day in the life'
        self.assertEqual(title(s), 'A Day in the Life', "The title of a day in the life should be A Day in the Life")

if __name__ == "__main__":
    unittest.main()
