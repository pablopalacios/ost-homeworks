from tkinter import *

ALL = N+S+W+E

class App(Frame):
    def __init__(self,master=None):
        Frame.__init__(self,master)
        self.master.rowconfigure(0,weight=1)
        self.master.columnconfigure(0,weight=1)
        self.grid(sticky=ALL)

        self.rowconfigure(0,weight=1,minsize=100)
        self.f1 = Frame(self,bg="yellow",name="frame 1")
        self.f1.grid(row=0,column=0,columnspan=2,sticky=ALL)
        self.f1.bind("<Button-1>",self.handler)

        self.rowconfigure(1,weight=1,minsize=100)
        self.f2 = Frame(self,bg="cyan",name="frame 2")
        self.f2.grid(row=1,column=0,columnspan=2,sticky=ALL)
        self.f2.bind("<Button-1>",self.handler)

        self.f3 = Frame(self,bg="blue",name="frame 3")
        self.f3.grid(row=0,column=2,columnspan=3,rowspan=2,sticky=ALL)
        self.entry = Entry(self.f3)
        self.entry.pack()
        self.text = Text(self.f3)
        self.text.pack(fill=BOTH,expand=True)

        self.rowconfigure(2, weight=0)
        for i in range(5):
            self.columnconfigure(i,weight=1)

        Button(self,text="Red",command=self.red).grid(row=2,column=0,sticky=ALL)
        Button(self,text="Green",command=self.green).grid(row=2,column=1,sticky=ALL)
        Button(self,text="Blue",command=self.blue).grid(row=2,column=2,sticky=ALL)
        Button(self,text="Black",command=self.black).grid(row=2,column=3,sticky=ALL)
        Button(self,text="Open",command=self.open_text).grid(row=2,column=4,sticky=ALL)        

    def open_text(self):
        try:
            f = open(self.entry.get())
            text = f.read() + "\n***\n"
            f.close()
            #self.text.replace(0.0,END,text)
            self.text.insert(0.0,text)
        #except FileNotFoundError:
        except IOError:
        #self.text.replace(0.0,END,"File not found.")
            pass
        
    def red(self):
        self.text.config(fg="red")
    def green(self):
        self.text.config(fg="green")
    def blue(self):
        self.text.config(fg="blue")
    def black(self):
        self.text.config(fg="black")
    def handler(self,event):
        text = "Widget: {0}\nX: {1}\ny: {2}\n***\n".format(event.widget,event.x,event.y)
        #self.text.replace(0.0,END,text)
        print(text)
        return "break"
        
root = Tk()
app = App(root)
app.mainloop()
