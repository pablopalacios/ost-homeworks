""" A function to count files in some directory by file type"""

import os

def file_type_counter(list_dir):
    d = {}
    for item in list_dir:
        i = os.path.splitext(item)
        ext = i[-1][1:]
        if ext:
            d[ext] = d.get(ext,0)+1
        else: 
            try:
                os.listdir(item)
            except WindowsError:
                d['No extension'] = d.get('No extension',0)+1
    ds = sorted(d,key=d.get, reverse=True)
    s = ''
    for ext in ds:
        s += '{0}: {1};\n'.format(ext,d[ext])
    return s