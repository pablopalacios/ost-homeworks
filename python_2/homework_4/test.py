from file_type_counter import file_type_counter

import unittest
import os
import shutil

class Testfile_type_counter(unittest.TestCase):
        
    def setUp(self):
        self.origdir = os.getcwd()
        self.dirname = os.mkdir('test_dir')
        os.chdir('./test_dir')
        # 2 .txt; 1 .html; 3 .css;
        self.fixture_files_with_extensions = ['file1.txt','file2.txt','file1.html','file1.css','file2.css','file3.css']
        # 1 dir; 2 .txt; 1 py; 
        self.fixture_dirs_and_files_with_extensions = (['dir1'],['file1.txt','file2.txt','file1.py'])
        # 2 files without extensions; 1 .py;
        self.fixture_files_without_extensions = ['file1','file2','file1.py']
        # 1 dir; 2 files without extensions; 1 .py;
        self.fixture_dirs_and_files_without_extensions = (['dir1'],['file1','file2','file1.py'])
        
    def test_files_with_extensions(self):
        # 2 .txt; 1 .html; 3 .css;
        for fn in self.fixture_files_with_extensions:
            f = open(fn,'w')
            f.close()
        fc = 'css: 3;\ntxt: 2;\nhtml: 1;\n'
        l = os.listdir('./')
        self.assertEqual(fc,file_type_counter(l))
    
    def test_dirs_and_files_with_extensions(self):
        # 1 dir; 2 .txt; 1 py;
        for dn in self.fixture_dirs_and_files_with_extensions[0]:
            os.mkdir(dn)
        for fn in self.fixture_dirs_and_files_with_extensions[1]:
            f = open(fn,'w')
            f.close()
        fc = 'txt: 2;\npy: 1;\n'
        l = os.listdir('./')
        self.assertEqual(fc, file_type_counter(l))
    
    def test_files_without_extensions(self):
        # 2 files without extensions; 1 .py;
        for fn in self.fixture_files_without_extensions:
            f = open(fn,'w')
            f.close()
        fc = 'No extension: 2;\npy: 1;\n'
        l = os.listdir('./')        
        self.assertEqual(fc, file_type_counter(l))
        
    def test_dirs_and_files_without_extensions(self):
        # 1 dir; 2 files without extensions; 1 .py;
        for dn in self.fixture_dirs_and_files_without_extensions[0]:
            os.mkdir(dn)
        for fn in self.fixture_dirs_and_files_without_extensions[1]:
            f = open(fn,'w')
            f.close()
        fc = 'No extension: 2;\npy: 1;\n'
        l = os.listdir('./')        
        self.assertEqual(fc, file_type_counter(l))
    
    def tearDown(self):
        os.chdir(self.origdir)
        shutil.rmtree('test_dir')
    
if __name__ == "__main__":
    unittest.main()
    
