"""
jotd stands for Joke of The Day.
It is a module that contains the function scheduler which can store
email messages in a database.
"""
import maildb
from email.utils import make_msgid
import datetime
import email
import itertools

def make_joke(recipient,date):
    """
    Creates and returns a email.Message instance with the Monty Python
    killer joke sketch.
    The 'recipient' argument must be a tuple of name and email values
    and 'date' argument must be a datetime.datetime instance.
    
    The email headers generated here are:
    - FROM
    - TO
    - SUBJECT
    - DATE
    - MESSAGE-ID
    
    The payload is the joke from the sketch.
    """
    MESSAGE = """Date: {date} -0000
From: Ernest Scribbler <website@example.com>
To: {name} <{email}>
Message-Id: {id}
Subject: The funniest joke in the world!

Wenn ist das Nunstück git und Slotermeyer? Ja! Beiherhund das Oder die Flipperwaldt gersput!
"""
    name, address = recipient
    # message_date could be done with format_datetime if Python 3.4 was the main interpreter
    # timezone is not counted here.
    message_date = date.strftime("%a, %d %b %Y %H:%M:%S") 
    message_id = make_msgid() 
    msg_str = MESSAGE.format(name=name,email=address,id=message_id,date=message_date)
    msg = email.message_from_string(msg_str)
    return msg

def scheduler(recipients,starttime,daycount):
    """
    Creates instances of email.Message with make_joke method addressed to 
    a list of 'recipients' for 'daycount' days and beggining at 'starttime'.
    The messages created are stored on a database.
    """
    firstDate = starttime + datetime.timedelta(days=1)
    dates = [firstDate + datetime.timedelta(days=day) for day in range(daycount)]
    product = itertools.product(recipients,dates)
    for c in product:
        msg = make_joke(*c)
        maildb.store(msg)

if __name__=="__main__":
    """Tests the time execution of scheduler"""
    import mysql.connector as msc
    import settings
    import time
    from database import login_info
    conn = msc.Connect(**login_info)
    curs = conn.cursor()
    daycounts = [1,5,10,50,100,500]
    for daycount in daycounts:
        curs.execute("DROP TABLE IF EXISTS message")
        conn.commit()
        curs.execute(settings.TBLDEF)
        conn.commit()
        start = time.time()
        scheduler(settings.RECIPIENTS,settings.STARTTIME,daycount)
        end = time.time()
        t = end-start
        print("Time of execution for {1} daycounts: {0} ({2} per day)".format(t,daycount,t/daycount))
    
    