"""
tests for joft.scheduler and joft.make_joke
"""
from database import login_info
import mysql.connector as msc
import unittest
import itertools
import email
from email.utils import parsedate_tz, mktime_tz, parseaddr
import datetime
import maildb
import jotd
from settings import RECIPIENTS, STARTTIME, DAYCOUNT, TBLDEF

oneDay = datetime.timedelta(days=1)
conn = msc.Connect(**login_info)
curs = conn.cursor()

class JOTD_TestCase(unittest.TestCase):
    def setUp(self):
        """Set up the message table, deleting all previous records.
        WARNING: ALL MESSAGES WILL BE ERASED!
        """
        curs.execute("DROP TABLE IF EXISTS message")
        conn.commit()
        curs.execute(TBLDEF)
        conn.commit()
        with open('email.txt') as f:
            self.msg = email.message_from_file(f)
            
        self.starttime = STARTTIME
        self.recipients = RECIPIENTS
        self.daycount = DAYCOUNT
                
    def test_cursor(self):
        """Verifies if curs can insert messages on the message table"""
        maildb.store(self.msg)
        curs.execute("""SELECT COUNT(*) FROM message""")
        msgct = curs.fetchone()[0]
        self.assertEqual(msgct,1,"maildb.store() is not able to insert messages into message table")

    def test_empty_table(self):
        """Verifies that the message table is empty"""
        curs.execute("SELECT COUNT(*) FROM message")
        msgct = curs.fetchone()[0]
        self.assertEqual(msgct,0,"The table message is not empty!")


    def test_make_joke_headers(self):
        """Compares if the message created by jotf.make_joke has the expected headers"""
        recipient = ('pablo','pablo@sleepy.com.br')
        date = datetime.datetime(2010,10,10,0,0,0)
        expected_msg = self.msg
        expected_msg['date'] = "Sun, 10 Oct 2000 00:00:00 +0000"
        expected_msg['to'] = 'pablo <pablo@sleepy.com.br>'
        observed_msg = jotd.make_joke(recipient,date)
        self.assertEqual(observed_msg['date'],expected_msg['date'])#,"The messages have different dates.")
        self.assertEqual(observed_msg['to'],expected_msg['to'],"The messages have different recipients")
        
    def test_make_joke_message_id(self):
        """Verifies if jotf.make_joke produces unique message-id headers for each message"""
        date = datetime.datetime(2010,10,10)
        recipients = [(letter,letter+'@sleepy.com.br') for letter in 'abcdefghijklmnopqrstuvwxyz']
        observed_msgs = [jotd.make_joke(recipient,date)['message-id'] for recipient in recipients]
        self.assertEqual(len(observed_msgs),len(set(observed_msgs)),"There are messages with the same message-id header.")

    def test_scheduler_mainDates(self):
        """Verifies if the first and last email have the correct date."""
        
        daycount = 10
        starttime = datetime.datetime(2010,10,10,23,0,0)
        recipients = [('pablo','pablo@sleepy.com.br')]
        jotd.scheduler(recipients, starttime, daycount)
        
        expected_beginDate = datetime.datetime(2010,10,11).date()
        expected_endDate = datetime.datetime(2010,10,20).date()
        
        results = maildb.msgs()
        
        observed_beginDate = datetime.datetime.fromtimestamp((mktime_tz(parsedate_tz(results[0][1]['date'])))).date()
        observed_endDate = datetime.datetime.fromtimestamp((mktime_tz(parsedate_tz(results[-1][1]['date'])))).date()
        
        self.assertNotEqual(len(results),0,"The table message is empty.")
        self.assertEqual(observed_beginDate,expected_beginDate,"The first message has an incorrect date.")
        self.assertEqual(observed_endDate,expected_endDate,"The last message has an incorrect date.")
        
    def test_email_count(self):
        """Verifies if the number of messages stored on the database equals to
        the number of expected messages (days*recipients)"""
        jotd.scheduler(self.recipients, self.starttime, self.daycount)
        curs.execute("SELECT COUNT(*) FROM message")
        observed_ct = curs.fetchone()[0]
        expected_ct = len(self.recipients)*self.daycount
        self.assertEqual(observed_ct,expected_ct,"Table message has the wrong number of rows.")
        
    
    def test_cartesian_product(self):
        """Verifies if each person on the recipients list got one message during daycount"""
        starttime = datetime.datetime(2010,10,10,23)
        dates = [(2010,10,11),(2010,10,12),(2010,10,13)]
        daycount = len(dates)
        recipients = [('pablo','pablo@sleepy.com.br'),
                      ('jordana','jordana@sleepy.com.br'),
                      ('kirby','kirby@sleepy.com.br')
                      ]
        expected_dates = [datetime.datetime(*date).date() for date in dates]
        expected_emails = [address[1] for address in recipients]
        combinations = itertools.product(expected_dates,expected_emails)
        expected = [result for result in combinations]

        jotd.scheduler(recipients, starttime, daycount)
        
        curs.execute("SELECT DISTINCT msgRecipient FROM message")
        observed_emails = [parseaddr(email[0])[1] for email in curs.fetchall()]
        
        curs.execute("SELECT DISTINCT msgDate FROM message")
        observed_dates = [date[0].date() for date in curs.fetchall()]
        observed_results = itertools.product(observed_dates,observed_emails)
        observed = [result for result in observed_results]
        
        self.assertEqual(len(observed),len(expected))#,"The table message is empty.")
        self.assertEqual(observed,expected)#,"The resulted date and address are not correct.")

if __name__=="__main__":
    unittest.main()
