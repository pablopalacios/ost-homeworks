"""
settings for jotd.scheduler.
"""
import datetime

RECIPIENTS = [('Pablo','pablo@sleepy.com.br'),
              ('Jordana','jordana@sleepy.com.br'),
              ('Steve','steve@sleepy.com.br'),
              ('Guido','guido@sleepy.com.br'),
              ('Linus','linus@sleepy.com.br')
          ]
STARTTIME = datetime.datetime(2010,10,10,0,0,0)
DAYCOUNT = 10

TBLDEF = """\
CREATE TABLE message (
    msgID INTEGER AUTO_INCREMENT PRIMARY KEY,
    msgMessageID VARCHAR(128),
    msgDate DATETIME,
    msgSenderName VARCHAR(128),
    msgSenderAddress VARCHAR(128),
    msgText LONGTEXT,
    msgRecipient VARCHAR(128)
)"""

