import shelve

def highscore(player,score):
    if type(score)!= int or type(player)!=str:
        raise TypeError('Invalid Input')
    table = shelve.open('highscore.shlf',writeback=True)
    s = table.get(player)
    if s:
        if score > s:
            table[player] = score
    else:
        table[player] = score
    return table[player]