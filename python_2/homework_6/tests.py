import unittest
import highscore
import shelve
import glob
import os

class TestHighScore(unittest.TestCase):
    def setUp(self):
        self.filename = 'V:/workspace/PersistentStorage_Homework/src/highscore.shlf'
        self.table = shelve.open(self.filename)
        self.fixture_player = "Joao"
                
    def testNewPlayer(self):
        new_score = 1
        self.assertEqual(highscore.highscore(self.fixture_player, new_score), 1)
            
    def testNewHighScore(self):
        score = 5
        highscore.highscore(self.fixture_player,score)
        new_score = 10
        self.assertEqual(highscore.highscore(self.fixture_player, new_score), 10)
    
    def testNoHighScore(self):
        score = 5
        highscore.highscore(self.fixture_player,score)
        new_score = 3
        self.assertEqual(highscore.highscore(self.fixture_player, new_score), score)
        
    def testBadScoreInput(self):
        bad_input = 1.3
        self.assertRaises(TypeError, highscore.highscore, self.fixture_player, bad_input)
    def testBadNameInput(self):
        bad_name = 123
        self.assertRaises(TypeError, highscore.highscore, bad_name, 1)

    def tearDown(self):
        self.table.close()
        shelve_files = glob.glob(self.filename + '*')
        for file in shelve_files:
            os.remove(file)
if __name__ =="__main__":
    unittest.main()