from tkinter import *

ALL = N+S+W+E

class App(Frame):
    def __init__(self,master=None):
        Frame.__init__(self,master)
        self.master.rowconfigure(0,weight=1)
        self.master.columnconfigure(0,weight=1)
        self.grid(sticky=ALL)

        self.rowconfigure(0,weight=1,minsize=100)
        f1 = Frame(self,bg="red",)
        f1.grid(row=0,column=0,columnspan=2,sticky=ALL)

        f3 = Frame(self,bg="blue",)
        f3.grid(row=0,column=2,columnspan=3,rowspan=2,sticky=W+E+N+S)

        self.rowconfigure(1,weight=1,minsize=100)
        f2 = Frame(self,bg="green",)
        f2.grid(row=1,column=0,columnspan=2,sticky=ALL)

        self.rowconfigure(2, weight=0)
        for i in range(5):
            self.columnconfigure(i,weight=1)
            Button(self,text="Button {0}".format(i+1)).grid(row=2,column=i,sticky=ALL)

root = Tk()
app = App(root)
app.mainloop()
