import unittest
from classFactory import build_row
import mysql.connector as msc
from database import login_info 

conn = msc.Connect(**login_info)
curs = conn.cursor()
TBLDEF = """\
CREATE TABLE user (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50),
    email VARCHAR(50)
)"""
        
class ClassFactoryTest(unittest.TestCase):
    
    def setUp(self):
        curs.execute("DROP TABLE IF EXISTS user")
        conn.commit()
        curs.execute(TBLDEF)
        self.users =((1,"Steve Holden","steve@holdenweb.com"),
                     (2,"Jordana Luft","jordana@sleepy.com.br"),
                     (3,"Pablo Palacios","pablo@sleepy.com.br"))
        
        for id,name,email in self.users:
            query = """INSERT INTO user (name,email) VALUES ("%s", "%s")""" % (name,email)
            curs.execute(query)
        self.User = build_row("user", "id name email")
        self.user = self.User(self.users[0])
                
    def test_not_empty(self):
        curs.execute('SELECT COUNT(*) FROM user')
        userct =curs.fetchone()[0]
        self.assertGreater(userct,0, "Database user table is empty.")
        self.assertEqual(userct,3, "Database user table has not the right rows number")
        
    def test_attributes(self):
        self.assertEqual(self.user.id,1)
        self.assertEqual(self.user.name, "Steve Holden")
        self.assertEqual(self.user.email, "steve@holdenweb.com")
        
    def test_repr(self):
        self.assertEqual(repr(self.user),
                         "user_record(1, 'Steve Holden', 'steve@holdenweb.com')")

    def test_retrieve_count(self):
        """Test if DataRow.retrieve() returns the right number of rows"""
        curs.execute('SELECT COUNT(*) FROM user')
        expected = curs.fetchone()[0]
        observed = len([user for user in self.user.retrieve(curs)])
        self.assertEqual(expected, observed, "DataRow is not retrieving all data")
        
    def test_retrieve_without_conditions(self):
        users = [self.User(user) for user in self.users]
        rows = self.user.retrieve(curs)
        for row,user in zip(rows,users):
            self.assertEqual(repr(row), repr(user))
        
    def test_retrieve_with_conditions(self):
        rows = self.user.retrieve(curs,condition="id=2")
        results = [row for row in rows]
        self.assertEqual(len(results),1,"len(results) must be 1!")
        expected_user = self.User(self.users[1])
        observed_user = results[0]
        self.assertEqual(repr(expected_user),repr(observed_user))


if __name__ == "__main__":
    unittest.main()