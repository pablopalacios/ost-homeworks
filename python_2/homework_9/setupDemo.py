"""
OST homework about unittest tests.
"""

import unittest
import tempfile
import shutil
import glob
import os

class FileTest(unittest.TestCase):
    
    def setUp(self):
        self.origdir = os.getcwd()
        self.dirname = tempfile.mkdtemp('testdir')
        os.chdir(self.dirname)
        
    def test_1(self):
        """" Verify creation of files is possible """
        files = {'this.txt', 'that.txt', 'the_other.txt'}
        for filename in files:
            f = open(filename, 'w')
            f.write('Some text\n')
            f.close()
            self.assertTrue(f.closed)
        dir_files = set(os.listdir(self.dirname))
        self.assertEqual(dir_files,files,'The directory that you are trying to create already has files')
                
    def test_2(self):
        """ Verify that te current directory is empty """
        self.assertEqual(glob.glob('*'), [], 'Directory not empty')
        
    def test_3(self):
        """Verify the size of a binary file"""
        f = open('binary', 'bw')
        f.write(b'\0'*1000000)
        f.close()
        s = os.stat('binary')
        self.assertEqual(s.st_size,1000000,"The file dosen't have 1000000 bytes.")
            
    def tearDown(self):
        os.chdir(self.origdir)
        shutil.rmtree(self.dirname)
            
if __name__ == '__main__':
    unittest.main()