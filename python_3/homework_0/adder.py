'''
Created on May 26, 2014

@author: ppalacio1
'''

def adder(x,y):
    "Add two integer"
    if type(x) != int or type(y) != int:
        raise TypeError("This function only add objects of type int.")
    return x+y