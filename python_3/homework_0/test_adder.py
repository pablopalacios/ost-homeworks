'''
Created on May 26, 2014
adder_error
@author: ppalacio1
'''
import unittest
from adder import adder

class Test(unittest.TestCase):

    def test_adder(self):
        """Verifies if adder returns the expected result."""
        self.assertEqual(adder(1,1),2,"1 + 1 must be equal 2.")
        self.assertEqual(adder(1,-1),0,"1+(-1) must be equal 0.")
        self.assertEqual(adder(-1,-1),-2,"(-1)+(-1) must be equal -2.")

    def test_adder_errors(self):
        """Tests that just int values can be added.
        Types tested: float, str, list, tuple, set and dict.
        """
        self.assertRaises(TypeError, adder, 1.0,1)
        self.assertRaises(TypeError, adder, 'e',1)
        self.assertRaises(TypeError, adder, [1],1)
        self.assertRaises(TypeError, adder, (1,1),1)
        self.assertRaises(TypeError, adder,{1},1)
        self.assertRaises(TypeError, adder,{1:1},1)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()