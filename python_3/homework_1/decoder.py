'''
Created on May 28, 2014

@author: ppalacio1
'''
from string import ascii_uppercase

def alphabator(iter):
    d = {number:letter for number,letter in zip(range(1,27),ascii_uppercase)}
    for elem in iter:
        try:
            yield d[elem]
        except KeyError:
            yield elem