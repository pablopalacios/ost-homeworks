'''
Created on May 29, 2014

@author: ppalacio1
'''

class Furnishing(object):
    def __init__(self,room):
        self.room = room
    
    def name(self):
        raise NotImplementedError("Furnishing must have a name.")

class Sofa(Furnishing):
    def name(self):
        return "Sofa"

class Bookshelf(Furnishing):
    def name(self):
        return "Bookshelf"

class Bed(Furnishing):
    def name(self):
        return "Bed"

class Table(Furnishing):
    def name(self):
        return "Table"

def map_the_home(lst):
    """Creates a dict based on a list of furnishings objects.
    The keys are based on the Furnishing.room attribute of each object
    on the list. The values are lists of furnishings subclasses.
    """
    home_map = {}
    for furnishing in lst:
        home_map[furnishing.room] = home_map.get(furnishing.room,[]) + [furnishing]
    return home_map

def counter(home):
    """Counts the furnishing by room and prints it on the screen.
    home_map must be a dict where the keys represent the room names and
    the values must be a list containing the furnishings."""
    d = {f:0 for f in ('Bed','Bookshelf','Sofa','Table')}
    for furnishing in home:
        d[furnishing.name()] += 1
    str = """Beds: {0[Bed]}
Bookshelves: {0[Bookshelf]}
Sofas: {0[Sofa]}
Tables: {0[Table]}""".format(d)
    
    return str

if __name__ == "__main__":
    home = []
    home.append(Bed('Bedroom'))
    home.append(Sofa('Living Room'))
    map_the_home(home) 
    print(counter(home))