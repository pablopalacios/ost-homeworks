'''
Created on May 29, 2014

@author: ppalacio1
'''
import unittest
from furnishings import Furnishing, Sofa, Bed, Table, counter, map_the_home

home = [Bed("Room 1"), 
        Bed("Room 2"),
        Sofa("Room 1")]

class TestFurnishing(unittest.TestCase):
    def test_furnishings(self):
        f = Furnishing("Bedroom")
        self.assertEqual(f.room, "Bedroom")
        self.assertRaises(NotImplementedError,f.name)
          
        self.assertEqual(Bed("Bedroom").room, "Bedroom")
        self.assertEqual(Bed("Bedroom").name(), "Bed")
        
        self.assertEqual(Sofa("Bedroom").room, "Bedroom")
        self.assertEqual(Sofa("Bedroom").name(), "Sofa")
            
        self.assertEqual(Table("Bedroom").room, "Bedroom")
        self.assertEqual(Table("Bedroom").name(), "Table")
            
    def test_map_the_home(self):
        home_map = map_the_home(home)
        self.assertEqual(len(home_map.keys()),2)
        keys = list(home_map.keys())
        keys.sort()
        self.assertEqual(keys,["Room 1","Room 2"])
        self.assertEqual(len(home_map["Room 1"]),2)
        self.assertEqual(len(home_map["Room 2"]),1)
        
    def test_counter(self):
        expected = """Beds: 2
Bookshelves: 0
Sofas: 1
Tables: 0"""
        self.assertEqual(counter(home),expected)

if __name__=="__main__":
    unittest.main()