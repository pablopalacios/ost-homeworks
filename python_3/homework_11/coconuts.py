'''
Created on May 27, 2014

@author: ppalacio1
'''
class Coconut(object):
    def __init__(self, name,weight):
        self.name = name
        self.weight = weight

class Inventory(object):
    coconuts = []
    def add_coconut(self,coconut):
        """Add a coconut to the inventory.
        coconut must be a Coconut instance."""
        if isinstance(coconut,Coconut):
            self.coconuts.append(coconut) 
        else:
            raise AttributeError("'%s' is not a coconut." % coconut)
        
    def total_weight(self):
        """Provides the total weight of coconuts"""
        return sum([coconut.weight for coconut in self.coconuts])

if __name__ == '__main__':
    pass