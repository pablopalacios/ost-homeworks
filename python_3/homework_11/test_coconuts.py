'''
Created on May 27, 2014

@author: ppalacio1
'''
import unittest
from coconuts import Coconut, Inventory

class Test(unittest.TestCase):

    def test_coconuts(self):
        c1 = Coconut("American",3.5)
        self.assertEqual(c1.name,"American")
        self.assertEqual(c1.weight,3.5)
        c2 = Coconut('South Asian',3)
        self.assertNotEqual(c1.weight,c2.weight)

    def test_add_coconut(self):
        i = Inventory()
        self.assertRaises(AttributeError,i.add_coconut,"This is not a coconut")
        
    def test_total_weight(self):
        south_asian = Coconut('South Asian',3)
        middle_eastern = Coconut("Middle Eastern",2.5)
        american = Coconut("American", 3.5)
        i = Inventory()
        i.add_coconut(south_asian)
        i.add_coconut(south_asian)
        i.add_coconut(middle_eastern)
        i.add_coconut(american)
        i.add_coconut(american)
        i.add_coconut(american)
        self.assertEqual(i.total_weight(),19)
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()