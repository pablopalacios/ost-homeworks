import random
import time
        
class Question:
    """
    Represents one math addition question.
    
    ATTRIBUTES:
    number: the question number
    x: first random number to be added
    y: second random number to be added
    right_answer: x + y
    user_answer: user answer
    time: the approximated time took by the user to answer
    result: if it is write or if it is wrong
    """
    def __init__(self, question_number, random_domain=(1,10)):
        """
        Make the question, print it to the user and collect the answer.
        If a random_domain is not specified the range of x and y will
        vary between 1 and 10.
        self.result returns True if the user answered right, otherwhise
        it will return False.
        """
        self.number = question_number
        self.x = random.randint(*random_domain)
        self.y = random.randint(*random_domain)
        self.right_answer= self.x+self.y
        
        start = time.time()
        self.user_answer = int(input("What is the sum of {0.x} and {0.y}? ".format(self)))
        end = time.time()
        
        self.time = round(end-start)
        self.result = (self.user_answer == self.right_answer)
        if self.result:
            self.right_or_wrong = "right"
        else:
            self.right_or_wrong = "wrong"

    def answer(self):
        """Return a quick answer string"""
        return "{0.user_answer} is {0.right_or_wrong}!".format(self)
    
    def __str__(self):
        return "Question #{0.number} took about {0.time} seconds to complete and was {0.right_or_wrong}.".format(self)

class MathQuiz:
    """Represents a mathquiz game
    It creates 5 questions based in a random image
    It need evaluate the average time answer
    It need evaluate time spent to answer the quiz
    atributos: average, totaltime, time per question"""
    def __init__(self,qs):
        self.questions = qs
        self.total_time = sum([q.time for q in self.questions])
        self.average = self.total_time/len(qs)
        
    def average_str(self):
        return "Your average time was {0.average} seconds per question".format(self)
    
    def total_time_str(self):
        return "You took {0.total_time} seconds to finish the quiz".format(self)
        
if __name__ == "__main__":
    questions = []
    for i in range(1,6):
        question = Question(i)
        print(question.answer())
        questions.append(question)
    mq = MathQuiz(questions)
    for question in questions:
        print(question) 
    print(mq.total_time_str())
    print(mq.average_str())