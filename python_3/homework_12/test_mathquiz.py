import unittest
import mathquiz
import time

def input_injection(message):
    """
    Replaces the input function to simulate a user answer
    This functions is only for test purpose.
    """
    time.sleep(2)
    return "4"

class TestMathQuiz(unittest.TestCase):

    def test_input_injection(self):
        """Verifies that input_injection works well"""
        d1 = time.time()
        i = input_injection("message")
        d2 = time.time()
        self.assertAlmostEqual(d2-d1,2,places=2)
        self.assertEqual(i,"4","input_injection must always return 4")

    def test_question_is_wrong(self):
        """Simulates a wrong answer."""
        random_domain = (1,1)
        q = mathquiz.Question(1,random_domain)
        self.assertEqual(q.number,1,"The question number is wrong")
        self.assertEqual(q.x,1,"X has a different value than expected")
        self.assertEqual(q.y,1,"Y has a different value than expected")
        self.assertEqual(q.right_answer,2,"Result must be 2 since x and y are equal 1 both")
        self.assertEqual(q.user_answer, 4, "Since input_injecton returns 4, answer must be 4 too")
        self.assertEqual(q.time,2,"Since we are using input_injection, time must be 2")
        self.assertFalse(q.result)
        self.assertEqual(q.right_or_wrong, "wrong")
        self.assertEqual(q.answer(), "4 is wrong!")
        self.assertEqual(q.__str__(),"Question #1 took about 2 seconds to complete and was wrong.")

    def test_question_is_right(self):
        """Simulates a correct answer."""
        random_domain = (2,2)
        q = mathquiz.Question(2,random_domain)
        self.assertEqual(q.number,2,"The question number is wrong")
        self.assertEqual(q.x,2,"X has a different value than expected")
        self.assertEqual(q.y,2,"Y has a different value than expected")
        self.assertEqual(q.right_answer,4,"Result must be 4 since x and y are equal 2 both")
        self.assertEqual(q.user_answer, 4, "Since input_injecton returns 4, answer must be 4 too")
        self.assertEqual(q.time,2,"Since we are using input_injection, time must be 2")
        self.assertTrue(q.result)
        self.assertEqual(q.right_or_wrong, "right")
        self.assertEqual(q.answer(),"4 is right!")
        self.assertEqual(q.__str__(),"Question #2 took about 2 seconds to complete and was right.")

    def test_mathquiz(self):
        """Verifies if MathQuiz is able to calculate average and total time
        for a set of questions."""
        qs = [mathquiz.Question(i) for i in range(5)]
        mq = mathquiz.MathQuiz(qs)
        self.assertEqual(mq.average,2)
        self.assertEqual(mq.total_time,10)
        self.assertEqual(mq.average_str(),"Your average time was 2.0 seconds per question")
        self.assertEqual(mq.total_time_str(),"You took 10 seconds to finish the quiz")
        
        
if __name__ == "__main__":
    mathquiz.input = input_injection # replaces the builtin input for this test only
    unittest.main()