'''
Created on May 28, 2014

@author: ppalacio1
'''
import unittest
from find_regex import find

class Test(unittest.TestCase):


    def test_find_regex(self):
        text = """In the 1950s, mathematician Stephen Cole Kleene described automata theory and formal language theory in a set of models using a notation called "regular sets" as a method to do pattern matching. Active usage of this system, called Regular Expressions, started in the 1960s and continued under such pioneers as David J. Farber, Ralph E. Griswold, Ivan P. Polonsky, Ken Thompson, and Henry Spencer."""
        obj = find(text)
        self.assertIsNotNone(obj, "The regex was not found in the string.")
        self.assertEqual(obj.group(),"Regular Expressions")
        self.assertEqual(obj.start(),231)
        self.assertEqual(obj.end(),250)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_find_regex']
    unittest.main()