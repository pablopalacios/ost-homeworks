'''
Created on May 30, 2014

@author: ppalacio1
'''
import struct
import datetime

fn = "./wireshark.bin"
with open(fn,'rb') as fp:
    header = struct.unpack("IHHiIII", fp.read(24))
    print(header)
    while True:
        try:
            ts_sec, ts_usec, incl_len, orig_len = struct.unpack("4I",fp.read(16))
            print(datetime.datetime.fromtimestamp(ts_sec),ts_usec)
            fp.read(incl_len)
        except struct.error:
            break
