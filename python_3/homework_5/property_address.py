'''
Created on Jun 3, 2014

@author: ppalacio1
'''
import re
import logging
import optparse
import configparser

config = configparser.RawConfigParser()
config.read('propertyaddress.cfg')

LOG_FORMAT = config.get('log','format') #"%(asctime)s - %(funcName)s - %(message)s"
logging.basicConfig(filename=config.get('log','output'), level=logging.INFO,format=LOG_FORMAT)

class InvalidLevel(Exception):
    pass

class StateError(Exception):
    pass

class ZipCodeError(Exception):
    pass

def validate_state(state):
    regex = re.compile(config.get('validators','state'))#'r"^[A-Z]{2}$"
    if not regex.match(state):
        logging.error("STATE exception")
        raise StateError()

def validate_zip_code(zip_code):
    regex = re.compile(config.get('validators','zip_code'))#'r"^\d{5}$"
    if not regex.match(zip_code):
        logging.error("ZIPCODE exception")
        raise ZipCodeError()

def set_logger_level(level):
    levels = {'DEBUG':logging.DEBUG,
            'INFO':logging.INFO,
            'WARNING':logging.WARNING,
            'ERROR': logging.ERROR,
            'CRITICAL':logging.CRITICAL
            }
    logger = logging.getLogger()
    try:
        logger.setLevel(levels[level])
    except KeyError:
        raise InvalidLevel()
        
class Address:
    def __init__(self, name, street_address, city, state, zip_code):
        self._name = name
        self.street_address = street_address
        self.city = city
        self._state = state
        self.zip_code = zip_code
        logging.info("Creating a new address")
        
    @property
    def name(self):
        return self._name
    
    @property
    def state(self):
        return self._state
    
    @state.setter
    def state(self, value):
        validate_state(value)
        self._state = value
    
    @property
    def zip_code(self):
        return self._zip_code
    
    @zip_code.setter
    def zip_code(self, value):
        validate_zip_code(value)
        self._zip_code = value
            
def main(options):
    Address(options.name,
            options.address,
            options.city,
            options.state,
            options.zip_code)
        
if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.add_option('-l','--level',default='INFO',dest='level',action='store')
    parser.add_option('-n','--name',dest='name',action='store')
    parser.add_option('-a','--address',dest='address',action='store')
    parser.add_option('-c','--city',dest='city',action='store')
    parser.add_option('-s','--state',dest='state',action='store')
    parser.add_option('-z','--zip_code',dest='zip_code',action='store')
    (options, args) = parser.parse_args()
    error_msg = "options -n, -a, -c, -s, -z are required"
    if options.level:
        try:
            set_logger_level(options.level)
        except InvalidLevel:
            parser.error("Invalid value for logging level")
    
    if not options.name:
        parser.error(error_msg)
    if not options.address:
        parser.error(error_msg)
    if not options.city:
        parser.error(error_msg)
    if not options.state:
        parser.error(error_msg)
    if not options.zip_code:
        parser.error(error_msg)
    
    try:
        main(options)
    except ZipCodeError:
        parser.error('option -z requires a valid US zip code')
    except StateError:
        parser.error('option -s requires a valid US state code')