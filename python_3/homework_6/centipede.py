'''
Created on May 31, 2014

@author: ppalacio1
'''

class Centipede:

    def __init__(self):
        self.__dict__['legs'] = []
        self.__dict__['stomach'] = []
    
    def __call__(self,arg):
        self.stomach.append(arg)
        
    def __str__(self):
        return ','.join(self.stomach)
    
    def __setattr__(self,key,value):
        if key == "legs" or key == "stomach":
            raise AttributeError("{0} is for internal use only".format(key))
        self.legs.append(key)
        self.__dict__[key] = value
        
    def __repr__(self):
        return ','.join(self.legs)