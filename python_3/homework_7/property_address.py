'''
Created on Jun 3, 2014

@author: ppalacio1
'''
import re

class StateError(Exception):
    pass

class ZipCodeError(Exception):
    pass

class Address:
    def __init__(self, name, street_address, city, state, zip_code):
        self._name = name
        self.street_address = street_address
        self.city = city
        self._state = state
        self.zip_code = zip_code
        
    @property
    def name(self):
        return self._name
    
    @property
    def state(self):
        return self._state
    
    @state.setter
    def state(self, value):
        regex = re.compile(r"^[A-Z]{2}$")
        if not regex.match(value):
            raise StateError()
        else:
            self._state = value
    
    @property
    def zip_code(self):
        return self._zip_code
    
    @zip_code.setter
    def zip_code(self, value):
        regex = re.compile(r"^\d{5}$")
        if not regex.match(value):
            raise ZipCodeError()
        else:
            self._zip_code = value
        