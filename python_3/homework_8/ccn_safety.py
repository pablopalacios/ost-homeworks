'''
Created on May 29, 2014

@author: ppalacio1
'''
import re 

def ccn_safety(text):
    regex = r"(\d{4}-){2}\d{4}"
    return re.sub(regex,"XXXX-XXXX-XXXX",text)