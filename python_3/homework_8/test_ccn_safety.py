'''
Created on May 29, 2014

@author: ppalacio1
'''
import unittest
from ccn_safety import ccn_safety
text = """Have you ever noticed, in television and movies, that phone numbers and credit cards
are obviously fake numbers like 555-123-4567 or 5555-5555-5555-5555? It is because a number
that appears to be real, such as 1234-5678-1234-5678, triggers the attention of privacy and 
security experts."""

class Test(unittest.TestCase):

    def test_ccn_safety(self):
        expected = """Have you ever noticed, in television and movies, that phone numbers and credit cards
are obviously fake numbers like 555-123-4567 or XXXX-XXXX-XXXX-5555? It is because a number
that appears to be real, such as XXXX-XXXX-XXXX-5678, triggers the attention of privacy and 
security experts."""
        result = ccn_safety(text)
        
        self.assertFalse("5555-5555-5555-5555" in result)
        self.assertFalse("1234-5678-1234-5678" in result)
        self.assertTrue("XXXX-XXXX-XXXX-5678" in result)
        self.assertTrue("XXXX-XXXX-XXXX-5555" in result)
        self.assertEqual(result,expected)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()