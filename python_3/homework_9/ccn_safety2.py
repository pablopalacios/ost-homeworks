'''
Created on May 29, 2014

@author: ppalacio1
'''
import re 

def ccn_safety(text):
    regex = re.compile(r"""
    \d{4}       # first 4 digits group
    (-\d{4}){3} # last 3 4 digits group""", re.VERBOSE)
    return regex.sub("CCN REMOVED FOR YOUR SAFETY",text)