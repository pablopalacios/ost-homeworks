'''
Created on May 29, 2014

@author: ppalacio1
'''
import unittest
from ccn_safety2 import ccn_safety
text = """Have you ever noticed, in television and movies, that phone numbers and credit cards
are obviously fake numbers like 555-123-4567 or 5555-5555-5555-5555? It is because a number
that appears to be real, such as 1234-5678-1234-5678, triggers the attention of privacy and 
security experts."""

class Test(unittest.TestCase):

    def test_ccn_safety(self):
        expected = """Have you ever noticed, in television and movies, that phone numbers and credit cards
are obviously fake numbers like 555-123-4567 or CCN REMOVED FOR YOUR SAFETY? It is because a number
that appears to be real, such as CCN REMOVED FOR YOUR SAFETY, triggers the attention of privacy and 
security experts."""
        result = ccn_safety(text)
        warning = "CCN REMOVED FOR YOUR SAFETY"
        self.assertFalse("5555-5555-5555-5555" in result)
        self.assertFalse("1234-5678-1234-5678" in result)
        self.assertTrue(warning in result)
        self.assertEqual(result.count(warning), 2)
        self.assertEqual(result,expected)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()