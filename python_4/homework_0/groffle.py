"""
Program for optimization. Python 4, Lesson 5.

Calculates the groffle speed of a knurl widget
of average density given by user input
"""

from math import log
from timeit import Timer
import array

def groffle_slow(mass, density):
    total = 0.0
    for i in range(10000):
        masslog = log(mass*density)
        total += masslog/(i+1)
    return total

def groffle_faster(mass, density):
    masslog = log(mass*density)
    total = sum(map(masslog.__truediv__,range(1,10001)))
    return total

mass = 2.5
density = 12.0

assert groffle_slow(mass,density) == groffle_faster(mass,density)

timer_slow = Timer("total = groffle_slow(mass, density)",
              "from __main__ import groffle_slow, mass, density")
timer_faster = Timer("total = groffle_faster(mass, density)",
                    "from __main__ import groffle_faster, mass, density")
slow = timer_slow.timeit(number=1000)
faster = timer_faster.timeit(number=1000)
print("time groffle_slow:", slow)
print("time groffle_faster:", faster)
print("faster/slow:", faster/slow)
