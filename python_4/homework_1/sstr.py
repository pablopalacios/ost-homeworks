'''
Created on Jun 15, 2014

@author: ppalacio1
'''
import itertools

class sstr(str):
        
    def __lshift__(self,n):
        shifts = n%len(self)
        s = self+self
        return sstr(s[shifts:shifts+len(self)])
    
    def __rshift__(self,n):
        shifts = n%len(self)
        p1 = self[-shifts:]
        p2 = ''
        if shifts > 0:
            p2 = self[:len(self)-shifts]
        return sstr(p1+p2)
        