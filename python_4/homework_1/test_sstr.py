'''
Created on Jun 15, 2014

@author: ppalacio1
'''
import unittest
from sstr import sstr

class Test_sstr(unittest.TestCase):

    def setUp(self):
        self.s1 = sstr("abcde")
    def test_lshift(self):
        self.assertEqual(self.s1<<0,'abcde')
        self.assertEqual(self.s1<<2,'cdeab')
        self.assertEqual(self.s1<<6,'bcdea')

    def test_rshift(self):
        self.assertEqual(self.s1>>0,'abcde')
        self.assertEqual(self.s1>>2,'deabc')
        self.assertEqual(self.s1>>5,'abcde')
        self.assertEqual(self.s1>>6,'eabcd')
        
    def test_expressions(self):
        self.assertTrue((self.s1>>5)<<5=='abcde')

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_str']
    unittest.main()