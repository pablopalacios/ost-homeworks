"""
Assuming t1, t2 and t3 being, respectively, the first, the second and the 
third thread to be started, t2 will be working in the directory of the 
very last finished os.chdir() call from any thread. So, if t3 called 
os.chdir() before t2 os.getcwd() call, t2 will return t3 directory. The 
result is the same if we change t3 by t1. 

So, it makes me think that all the threads share the same working 
directory at a given moment. Since one of the benefits of multithreaded 
program is to share resources between threads, the working directory is 
one of this shared resources.

This can be proved by making each thread create a set of files in a 
different directory. If each thread holds its own working directory, the 
result of this experiment would be two directories with no messed content 
(it means, file created by t1 would not be in t3 directory). But, when we 
run this experiment, we see that the directories have a unpredictable 
content: sometimes the file N created by t1 (or t3) is in the right place 
but sometimes it is not.
"""
import os, time
import threading

PATH = 'V:/workspace/Python4_Homework10/src/'

def chdir(path,name):
    os.chdir(path)
    for i in range(10):
        with open("file_{0}_{1}".format(i,name),'w') as f:
            f.write(name)
        print(name, os.getcwd())
    
def getdir():
    for i in range(10):
        time.sleep(0.1)
        print("t2",os.getcwd())

os.chdir(PATH)
print("# start test")
t1 = threading.Thread(target=chdir,args=(PATH+'t1','t1'))
t1.start()
t2 = threading.Thread(target=getdir)
t2.start()
t3 = threading.Thread(target=chdir, args=(PATH+'t3','t3'))
t3.start()

    
