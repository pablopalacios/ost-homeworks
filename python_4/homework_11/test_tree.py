import unittest
import tree

class Test(unittest.TestCase):
    def setUp(self):
        self.tree = tree.Tree("D",0)
    
    def test_insert(self):
        self.assertIsNone(self.tree.left)
        self.assertIsNone(self.tree.right)
        self.tree.insert("C",1)
        self.tree.insert("E",2)
        self.assertTrue(isinstance(self.tree.left,tree.Tree))
        self.assertTrue(isinstance(self.tree.right,tree.Tree))
        with self.assertRaises(ValueError):
            self.tree.insert("D",1)
        
    def test_walk_and_find(self):
        # testing Tree.walk
        for number, letter in enumerate("BJQKFAC"):
            self.tree.insert(letter,number+1)
        self.assertEqual(len(list(self.tree.walk())),8)
        # testing Tree.find
        for number, letter in enumerate("BJQKFAC"):
            self.assertEqual(self.tree.find(letter),number+1)
        with self.assertRaises(KeyError):
            self.tree.find("Kirby")
    
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()