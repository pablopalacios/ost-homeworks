class Tree:
    def __init__(self, key, value):
        "Create a new Tree object with empty L & R subtrees."
        self.key = key
        self.value = value
        self.left = self.right = None
    def insert(self, key, value):
        "Insert a new element into the tree in the correct position"
        if key < self.key:
            if self.left:
                self.left.insert(key,value)
            else:
                self.left = Tree(key,value)
        elif key > self.key:
            if self.right:
                self.right.insert(key,value)
            else:
                self.right = Tree(key,value)
        else:
            raise ValueError("Attempt to insert duplicate value")
    
    def walk(self):
        "Generates the tree objects sorted"
        if self.left:
            for n in self.left.walk():
                yield n
        yield self
        if self.right:
            for n in self.right.walk():
                yield n

    def find(self, key):
        "Retrieve the value of a key"
        for n in self.walk():
            if n.key == key:
                return n.value
        raise KeyError("There is no key %r in this tree." % key)    
    