'''
Created on Jun 10, 2014

@author: ppalacio1
'''
import types
class Composable:
    def __init__(self, f):
        "Store reference to proxied function."
        self.func = f
    def __call__(self, *args, **kwargs):
        "Proxy the function, passing all arguments through."
        return self.func(*args, **kwargs)
    def __mul__(self, other):
        "Return the composition of proxied and another funtion."
        if type(other) is Composable:
            def anon(x):
                return self.func(other.func(x))
            return Composable(anon)
        elif type(other) is types.FunctionType:
            def anon(x):
                return self.func(other(x))
            return Composable(anon)
        raise TypeError("Illegal operands for multiplication")
    
    def __pow__(self, power):
        "Return the composition of proxied function with itself power times"
        if type(power) is not int:
            raise TypeError("Illegal operands for power")
        elif power <= 0:
            raise ValueError("You must provide a positive integer number")
        else:
            base = self
            r = base
            while power-1 > 0:
                r*= base
                power-=1
            return r

    def __repr__(self):
        return "<Composable function {0} at 0x{1:X}>".format(
                            self.func.__name__, id(self))