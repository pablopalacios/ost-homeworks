'''
Created on Jun 10, 2014

@author: ppalacio1
'''
import unittest
from composable import Composable

def add1(x):
    return x + 1

def reverse(s):
    "Reverses a string using negative-stride sequencing."
    return s[::-1]

def square(x):
    "multiplies a number by itself."
    return x*x

class ComposableTestCase(unittest.TestCase):

    def test_inverse(self):
        reverser = Composable(reverse)
        nulltran = reverser * reverser
        tran = reverser**3
        for s in "", "a", "0123456789", "abcdefghijklmnopqrstuvwxyz":
            self.assertEquals(nulltran(s),s)
            self.assertEquals(tran(s),s[::-1])
        
            
    def test_square(self):
        squarer = Composable(square)
        po4 = squarer * square
        _po4 = squarer**2
        for v,r in ((1,1), (2,16), (3,81)):
            self.assertEqual(po4(v),r)
            self.assertEqual(_po4(v),r)
    
    def test_add1(self):
        f = Composable(add1)
        nf = f**3
        for v,r in ((1,4),(2,5),(3,6)):
            self.assertEqual(nf(v),r)
        
         
    def test_exceptions(self):
        fc = Composable(square)
        with self.assertRaises(TypeError):
            fc = fc*3
        with self.assertRaises(TypeError):
            fc = square*fc
        with self.assertRaises(TypeError):
            fc = fc**square
        with self.assertRaises(ValueError):
            fc = fc**-3

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()