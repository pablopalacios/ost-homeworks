'''
Question URL: http://stackoverflow.com/q/23966164/3689669
'''
question = """Simplest way to have my python program store/retrieve 
information from an online database?

My python program is a lab chemical program, it will be installed 
on many different computers in several different labs. All of these 
programs will need to be able to store information to the same online 
database and retrieve information from this same online database. 
What is the SIMPLEST way for me to achieve this?

I have considered using MySQLdb but the problem is:
I am running python on the CLIENT not the SERVER, I have to edit the 
my.ini file on the SERVER (which I don't have access to) in order to 
allow remote connections from CLIENTS.

What are some simple options here?
"""
answer = """First of all you need a database host that lets you connect 
remotely, so, before you contract a host, check with them if you can 
access the database remotely.

If your host does it, you just need to get the information with them to 
connect to the database server (usually, the host address, a port, a 
database name, an user and a password).

Then, create a python file (eg. database.py) to store these information.

Finally, in your program, import these information to create a connection.

Typically you would have:
"""
#########################
"""
database.py: provides information to make a database connection
"""
login_info = {
    "host":"host_adress",
    "user":"user_name",
    "password":"very_secret_password", 
    "database":"database_name",
    "port": 3306
    }
#########################

"""Of course, change the dict values to that you got with your host.
In your program:"""

#########################
"""
main_program.py: a chemical program
"""
from database import login_info
import mysql.connector

conn = mysql.connector.Connect(**login_info)
curs = conn.cursor()

# STORE
curs.execute("""INSERT INTO table_name(row1,row2) VALUES("foo","bar")""")
conn.commit()

# RETRIVE
curs.execute("""SELECT * FROM table_name""")
results = curs.fetchall()
for result in results:
    print(result)
#########################

"""Note that I'm using the MySQL Connector/Python driver (and not the MySQLdb) 
that you can get [here][1]. The MySQL connector has a satisfactory [documentation][2] 
with code examples that may help you.
[1]: https://dev.mysql.com/downloads/connector/python/
[2]: https://dev.mysql.com/doc/connector-python/en/index.html
"""
comments = """
Other: What are the pros/cons of MySQL vs MySQLdb module? 
Me: Both works almost in the same way, since both are PEP 249 compliant. But two 
advantages from MySQL driver over MySQLdb are: Python 3 support and no dependencies 
on installation (MySQLdb requires MySQL client libraries, while MySQL driver just 
need Python installed)
Other: Perfect, I think I will switch over to using MySQL driver. Thanks for your help, 
it's much appreciated!
"""
