'''
Created on Jun 10, 2014

@author: ppalacio1
'''

class array:
    def __init__(self,X,Y,Z):
        "Create an array"
        self._data = {}
        self._x = X
        self._y = Y
        self._z = Z
        
    def __getitem__(self,key):
        "returns the element"
        x, y, z = self._validate_key(key)
        try:
            return self._data[x,y,z]
        except KeyError:
            return 0
        
    def __setitem__(self,key,value):
        "sets an element"
        x, y, z = self._validate_key(key)
        self._data[x,y,z] = value
        
    def _validate_key(self, key):
        "Valdiates a key"
        x, y, z = key
        if (0 <= x < self._x and
            0 <= y < self._y and
            0 <= z < self._z):
            return key
        raise KeyError("Subscript out of range")
        