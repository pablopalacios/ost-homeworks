'''
Created on Jun 10, 2014

@author: ppalacio1
'''
import unittest
import arr
import itertools

def generate_keys():
    """Create a generator of keys for a 3d array with all 
    possibility of invalid keys with the numbers -1,1,10
    """
    key = (-1,1,10)
    product_keys = itertools.product(key,key,key)
    keys = (k for k in product_keys if -1 in k or 10 in k)
    return keys

class TestArray(unittest.TestCase):

    def test_zeroes(self):
        for N in range(4):
            a = arr.array(N,N,N)
            for i in range(N):
                for j in range(N):
                    for k in range(N):
                        self.assertEqual(a[i,j,k],0)
                    
    def test_identity(self):
        for N in range(4):
            a = arr.array(N,N,N)
            for i in range(N):
                a[i,i,i] = 1
            for x in range(N):
                for y in range(N):
                    for z in range(N):
                        self.assertEqual(a[x,y,z], x==y==z)

    def _index(self ,a,x,y,z):
        return a[x,y,z]
    
    def test_key_validity(self):
        a = arr.array(10,10,10)
        for key in generate_keys():
            self.assertRaises(KeyError, self._index, a ,key[0], key[1], key[2])
        
        
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_zeroes']
    unittest.main()