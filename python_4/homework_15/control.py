'''
control.py: Creates queues, stars output and worker processes,
            and pushes inputs into the input queue.n
'''
from string import ascii_lowercase
from multiprocessing import Queue, JoinableQueue
from output import OutThread
from worker import WorkerThread
import random

if __name__ == '__main__':
    WORKERS = 10
    
    inq = JoinableQueue(maxsize=int(WORKERS*1.5))
    outq = Queue(maxsize=int(WORKERS*1.5))
    
    ot = OutThread(WORKERS, outq)
    ot.start()
    
    for i in range(WORKERS):
        w = WorkerThread(inq, outq)
        w.start()
    instring = ''.join([random.choice(ascii_lowercase) for i in range(1000)])
    for work in enumerate(instring):
        inq.put(work)
    for i in range(WORKERS):
        inq.put(None)
    inq.join()
    print("Control process terminating")