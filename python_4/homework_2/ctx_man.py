'''
Created on Jun 15, 2014

@author: ppalacio1
'''

class ctx_man:
    def __enter__(self):
        pass
    
    def __exit__(self, exc_type, exc_value, exc_tb):
        if exc_type == type(ValueError()):
            return True
        return False

if __name__=="__main__":
    with ctx_man():
        float("asdf")
    with ctx_man():
        3/0