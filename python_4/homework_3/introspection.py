from inspect import getfullargspec as get_args 
from inspect import formatargspec as format_args
from inspect import getmembers, isfunction
import smtplib

functions = getmembers(smtplib, isfunction)
for name,function in functions:
    print(name,format_args(*get_args(function)))