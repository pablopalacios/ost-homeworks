'''
control.py: Creates queues, statrs output and worker threads,
and pushes inputs into the input queue.n
'''
from string import ascii_lowercase
from queue import Queue
from output import OutThread
from worker import WorkerThread
import random, timeit

def main():
    WORKERS = 10

    inq = Queue(maxsize=int(WORKERS*1.5))
    outq = Queue(maxsize=int(WORKERS*1.5))

    ot = OutThread(WORKERS, outq)
    ot.start()

    for i in range(WORKERS):
        w = WorkerThread(inq, outq)
        w.start()
    instring = ''.join([random.choice(ascii_lowercase) for i in range(1000)])
    for work in enumerate(instring):
        inq.put(work)
    for i in range(WORKERS):
        inq.put(None)
    inq.join()
    print("Control thread terminating")

def upper():
    instring = ''.join([random.choice(ascii_lowercase) for i in range(1000)])
    return instring.upper()

print("time with threads:",timeit.timeit("main()","from __main__ import main",number=3))
print("time with one thread:",timeit.timeit("upper()","from __main__ import upper",number=3))
