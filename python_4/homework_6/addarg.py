def addarg(arg):
    def decorator(f):
        def wrapper(*args,**kw):
            args = [arg] + list(args)
            return f(*args,**kw)
        return wrapper
    return decorator