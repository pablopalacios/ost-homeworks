'''
Created on Jun 13, 2014

@author: ppalacio1
'''
import unittest
from addarg import addarg

@addarg(1)
def prargs(*args):
    return args

@addarg('spam')
def make_sandwich(*ingredients, **special_ingredients):
    sandwich = []
    for ingredient in ingredients:
        sandwich.append(ingredient)
    for ingredient in special_ingredients:
        sandwich.append(special_ingredients[ingredient])
    return sandwich

class TestDecorators(unittest.TestCase):

    def test_addarg(self):
        self.assertEqual(prargs(2,3),(1,2,3))
        self.assertEqual(prargs(),(1,))
        
        ingredients = ['egg','cheese','bacon']
        special_ingredients = {'bird':'albatroz'} 
        sandwich = make_sandwich(*ingredients,**special_ingredients)
        self.assertEqual(sandwich,['spam','egg','cheese','bacon','albatroz'])
        
        self.assertEqual(make_sandwich(bird='albatroz'),['spam','albatroz'])
        self.assertEqual(make_sandwich(),['spam'])

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_addarg']
    unittest.main()