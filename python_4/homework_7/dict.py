class Dict(dict):
    def __init__(self,default):
        dict.__init__(self)
        self.default_value = default
        
    def __getitem__(self,key):
        try:
            return dict.__getitem__(self,key)
        except KeyError:
            return self.default_value
        
