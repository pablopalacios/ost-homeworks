'''
Created on Jun 13, 2014

@author: ppalacio1
'''
import unittest
from dict import Dict

class Test(unittest.TestCase):

    def test_dict(self):
        d = Dict("spam spam spam")
        self.assertEqual(d['sandwich'],"spam spam spam")
        d['sandwich'] = "spam egg spam"
        self.assertEqual(d['sandwich'],"spam egg spam")
                         
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_dict']
    unittest.main()