from timeit import timeit
import random

def make_lst():
    lst = [random.random() for i in range(1000000)]
    return lst

def make_gen():
    gen = (random.random() for i in range(1000000))
    return gen


print("list() function:")
print("gen:", timeit("list(make_gen())","from __main__ import make_gen",number=20))
print("lst:", timeit("list(make_lst())","from __main__ import make_lst",number=20))

print("list comprehension:")
print("gen:", timeit("[i for i in make_gen()]","from __main__ import make_gen",number=20))
print("lst:", timeit("[i for i in make_lst()]", "from __main__ import make_lst",number=20))