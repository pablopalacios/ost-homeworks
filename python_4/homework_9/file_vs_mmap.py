import mmap, os
from timeit import timeit
SIZE = 10000000

try:
    os.remove("mmap_method")
    os.remove("file_method")
except:
    pass

def mmap_method(chunk_size):
    with open("mmap_method","w+b") as f:
        f.write(b"0"*SIZE)

    with open("mmap_method",'r+b') as f:
        mapf = mmap.mmap(f.fileno(), 0)
        for i in range(0,SIZE,chunk_size):
            mapf[i:i+chunk_size] = b'm'*chunk_size
        mapf.close()

def file_method(chunk_size):
    with open("file_method","w+b") as f:
        f.write(b"0"*SIZE)

    with open("file_method",'r+b') as f:
        for i in range(0,SIZE,chunk_size):
            f.write(b'0'*chunk_size)

CHUNKS = (10**0,10**1,10**2,10**3,10**4,10**5,10**6,10**7)
for chunk in CHUNKS:
    print("Chunk size:",chunk)
    t1 = timeit("mmap_method({})".format(chunk),"from __main__ import mmap_method",number=10)
    t2 = timeit("file_method({})".format(chunk),"from __main__ import file_method",number=10)
    print("mmap_method:",t1)
    print("file_method:",t2)
    print('')
